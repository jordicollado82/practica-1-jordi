const io = require('../io'); // cargamos la librería que hemos creado de input/output
const crypt = require('../crypt'); // cargamos la librería que hemos creado de encriptacion
const requestJson = require('request-json');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechujcg12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; // cargo la apikey guardada como variable de entorno en .env

function loginUserV1(req,res){

      //console.log("Usuario y contraseña a loggear: " + req.body.email + "/" + req.body.password);

      var users = require('../usuarios.json');

      // busqueda del usuario
      for(user in users) {
        if ((users[user].email == req.body.email)&&(users[user].password == req.body.password)){
            users[user].logged = true;
            var userid = users[user].id;
            break;
         }
      }

      if (userid) {
         io.writeUserDataToFile(users);
         res.send({"msg":"Login correcto","id":userid});
      }else{
         res.send({"msg":"Login incorrecto"});
      }

}

function loginUserV2(req,res){
      console.log("POST /apitechu/v2/login");

      var query = "q=" + JSON.stringify({"email": req.body.email});

      var httpClient = requestJson.createClient(baseMLABUrl);
      httpClient.get("user?" + query + "&" + mLabAPIKey,
          function(err, resMlab, body) {
              if (body.length > 0){
                  if (!crypt.checkPassword(req.body.password,body[0].password)){
                      var response = {
                         "msg" : "Login incorrecto"
                      }
                      res.status(401);
                      res.send(response);
                  }else{

                      var id = Number.parseInt(body[0].id);
                      query = "q=" + JSON.stringify({"id": id});

                      var putBody = '{"$set":{"logged":true}}';

                      httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                          function(errLoggin, resMLabLoggin, bodyLoggin) {
                                var response = {
                                    "msg" : "Login correcto",
                                    "idUsuario" : body[0].id
                                }
                                res.send(response);
                          }
                      )
                  }
              }else{
                  var response = {
                     "msg" : "Login incorrecto"
                  }
                  res.status(401);
                  res.send(response);
              }
          }
      );
}


function logoutUserV1(req,res){

      //console.log("Usuario a desconectar: " + req.params.id);

      var users = require('../usuarios.json');

      // busqueda del usuario
      for(user in users) {
        if ((users[user].id == req.params.id)&&(users[user].logged === true)){
             var logged = true;
             delete users[user]["logged"];
        }
        break;
      }

      if (logged) {
          io.writeUserDataToFile(users);
          res.send({"msg":"Logout correcto","id":req.params.id});
      }else{
          res.send({"msg":"Logout incorrecto"});
      }

}


function logoutUserV2(req,res){

  var id = Number.parseInt(req.params.id);
  var query = "q=" + JSON.stringify({"id": id});

  var httpClient = requestJson.createClient(baseMLABUrl);
  httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMlab, body) {
          if (body.length > 0){
              if (body[0].logged === true){
                 id = Number.parseInt(body[0].id);
                 query = "q=" + JSON.stringify({"id": id});
                 var putBody = '{"$unset":{"logged":""}}'
                 httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                     function(errLogout, resMlablogout, bodylogout) {
                         var response = {
                             "msg" : "Logout correcto",
                             "idUsuario" : body[0].id
                         }
                     }
                 );
              }else{
                var response = {
                  "msg" : "Logout incorrecto"
                }
                res.status(401);
                res.send(response);
              }
          }else{
              var response = {
                "msg" : "Logout incorrecto"
              }
              res.status(401);
              res.send(response);
          }
      }
  );

}

module.exports.loginUserV1 = loginUserV1;
module.exports.loginUserV2 = loginUserV2;
module.exports.logoutUserV1 = logoutUserV1;
module.exports.logoutUserV2 = logoutUserV2;
