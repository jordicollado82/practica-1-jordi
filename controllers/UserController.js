const io = require('../io'); // cargamos la librería que hemos creado de input/output
const crypt = require('../crypt'); // cargamos la librería que hemos creado de encriptacion
const requestJson = require('request-json');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechujcg12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; // cargo la apikey guardada como variable de entorno en .env

function getUsersV1(req,res){

    console.log("GET /apitechu/v1/users");

    //res.sendFile("usuarios.json", {root:__dirname}); //enviaria el fichero directamente
    var resultado = {}; //creo una estructura vacia
    var users = require('../usuarios.json'); //cargo el json en la variable users

    if (req.query.$count == "true"){
        resultado.count = users.length //incluyo en resultado etiqueta count con valor
    }

    if (typeof req.query.$top != "undefined") {
        resultado.users = users.slice(0, req.query.$top); //incluyo en resultado etiqueta users con el json de usuarios
    }else{
        resultado.users = users;
    }

    res.send(resultado); //envío users
}

function getUsersV2(req,res){

    console.log("GET /apitechu/v2/users");

    var httpClient = requestJson.createClient(baseMLABUrl);
    console.log("client created");

    httpClient.get("user?" + mLabAPIKey,
        function(err, resMlab, body) {
            var response = !err ? body : {
              "msg" : "Error obteniendo usuarios"
            }

            res.send(response);
        }
    );

}

function getUserByIdV2(req,res){

    console.log("GET /apitechu/v2/users/:id");

    var id = Number.parseInt(req.params.id);
    query = "q=" + JSON.stringify({"id": id});

    var httpClient = requestJson.createClient(baseMLABUrl);
    httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMlab, body) {
          if (err) {
            var response = {
              "msg" : "Error obteniendo usuario"
            }
            res.status(500);
          } else {
            if (body.length > 0){
              var response = body[0];
            } else {
              var response = {
                "msg" : "Usuario no encontrado"
              }
              res.status(404);
            }
          }
          res.send(response);
        }
    );

}

function deleteUsersV1(req,res){

      console.log("DELETE /apitechu/v1/users/:id");
      console.log("La id del usuario a borrar es " + req.params.id);

      var users = require('../usuarios.json');
      var userLocate = false;

      // bucle for of
      var iterator = users.entries();
      var i = 0;
      for(user of iterator) {
        console.log(user[1]);
        if (user[1].id == req.params.id){
            userLocate = true;
            i = user[0];
            break;
         }
      }

      // bucle for
      /*
      for(i=0; i<users.length; i++) {
         if (users[i].id == req.params.id){
            userLocate = true;
            break;
         }
      }*/

      // bucle for in
      /*
      var i = 0;
      for(user in users) {
        console.log(users[user].id);
        if (users[user].id == req.params.id){
            userLocate = true;
            i = user;
            break;
         }
      }*/

      // bucle for each
      /*
      users.forEach(function(user,index){
        if (user.id == req.params.id){
            userLocate = true;
            i = index;
         }
      })*/

      // findIndex
      /*
      function locateUser(users){
        return users.id == req.params.id;
      }

      var i = users.findIndex(locateUser);
      console.log(i);
      if (i != -1){
         userLocate = true;
      }*/


      if (userLocate) {
         users.splice(i,1);
         io.writeUserDataToFile(users);
         res.send({"msg":"Usuario con id: " + req.params.id +" borrado"});
      }else{
         res.send({"msg":"Usuario con id: " + req.params.id +" no localizado"});
      }

}

function createUsersV1(req,res){

          console.log("POST /apitechu/v1/users");

          console.log(req.body);
          console.log(req.body.first_name);
          console.log(req.body.last_name);
          console.log(req.body.email);

          var newUser = {
            "first_name" : req.body.first_name,
            "last_name" : req.body.last_name,
            "email" : req.body.email,
          }

          console.log(newUser);

          var users = require('../usuarios.json');
          users.push(newUser);
          console.log("Usuario añadido al array");

          io.writeUserDataToFile(users);
          res.send({"msg":"usuario creado"});

}

function createUsersV2(req,res){

          console.log("POST /apitechu/v2/users");

          console.log(req.body);
          console.log(req.body.id);
          console.log(req.body.first_name);
          console.log(req.body.last_name);
          console.log(req.body.email);
          console.log(req.body.password);

          var newUser = {
            "id" : req.body.id,
            "first_name" : req.body.first_name,
            "last_name" : req.body.last_name,
            "email" : req.body.email,
            "password" : crypt.hash(req.body.password)
          }
          var httpClient = requestJson.createClient(baseMLABUrl);
          console.log("client created");

          httpClient.post("user?" + mLabAPIKey, newUser,
            function(){
                console.log("Usuario creado");
                res.status(201).send({"msg":"Usuario creado"});
            }
          );
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.deleteUsersV1 = deleteUsersV1;
module.exports.createUsersV1 = createUsersV1;
module.exports.createUsersV2 = createUsersV2;
