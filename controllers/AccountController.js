const requestJson = require('request-json');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechujcg12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY; // cargo la apikey guardada como variable de entorno en .env

function getAccountByIdV1(req,res){

    console.log("GET /apitechu/v1/accounts/:idUsuario");

    var idusuario = Number.parseInt(req.params.idusuario);
    query = "q=" + JSON.stringify({"idUsuario": idusuario});

    var httpClient = requestJson.createClient(baseMLABUrl);
    httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMlab, body) {
          if (err) {
            var response = {
              "msg" : "Error obteniendo cuentas"
            }
            res.status(500);
          } else {
            if (body.length > 0){
              var response = body;
            } else {
              var response = {
                "msg" : "Cuentas de usuario no encontradas"
              }
              res.status(404);
            }
          }
          res.send(response);
        }
    );

}

module.exports.getAccountByIdV1 = getAccountByIdV1;
