const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should(); // cargamos el mecanismo should para hacer aserciones

describe("First test",  //definimos la estructura de suites de pruebas unitarias
  function() {
    it('Test that duckduckgo works', function(done) {
        chai.request('http://www.duckduckgo.com') //defino un path/dominio base
            .get('/')
            .end(
              function(err,res){
                  console.log("Request finished");
                  // console.log(res);
                  // console.log(err);
                  done();// le indico al framwork que ya he preparado las aserciciones y todo para hacer la prueba para comenzar
              }
            )
      }
    )
  }
)

describe("Test de API de usuarios",  //definimos la estructura de suites de pruebas unitarias
  function() {
    it('Test that user api says hello', function(done) {
        chai.request('http://localhost:3000') //defino un path/dominio base
            .get('/apitechu/v1/hello')
            .end(
              function(err,res){
                  console.log("Request finished");
                  res.should.have.status(200); //el código de respuesta debe ser un 200
                  res.body.msg.should.be.eql("Hola desde API TechU"); //el mensaje de respuesta debe ser el esperado
                  done();// le indico al framwork que ya he preparado las aserciciones y todo para hacer la prueba para comenzar
              }
            )
      }
    ),
    it('Test that user api return user list', function(done) {
        chai.request('http://localhost:3000') //defino un path/dominio base
            .get('/apitechu/v1/users')
            .end(
              function(err,res){
                  console.log("Request finished");
                  res.should.have.status(200); //el código de respuesta debe ser un 200
                  res.body.users.should.be.a('array'); // en la respuesta 'users' debe ser un array

                  for (user of res.body.users) {
                    user.should.have.property('id');
                    user.should.have.property('email');
                    user.should.have.property('password');
                  }

                  done();// le indico al framwork que ya he preparado las aserciciones y todo para hacer la prueba para comenzar
              }
            )
      }
    )
  }
)
