require('dotenv').config(); // busca el archivo .env y carga las variables de entorno definidas, en este caso el apikey


const express = require('express'); // cargamos el framework
const app = express(); // inicializa el framwork


var enableCORS = function(req, res, next) {
    res.set("Access-Control-Allow-Origin", "*");
    res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

    // This will be needed.
    res.set("Access-Control-Allow-Headers", "Content-Type");

    next();
}

app.use(express.json()); // Añade un preprocesador para que pueda recoger el body como json ya que sino llega como undefined.
app.use(enableCORS);

const userController = require('./controllers/UserController'); // cargamos los controladores de usuarios
const authController = require('./controllers/AuthController'); // cargamos los controladores de login/logout
const accountController = require('./controllers/AccountController'); // cargamos los controladores de login/logout

const port = process.env.PORT || 3000  //definimos el puerto
app.listen(port);

console.log("API escuchando en el puerto BIP BIP " + port);

app.get( "/apitechu/v1/hello",    //registro una ruta para una peticion get
    function(req, res){ //parámetros de entrada y salida
      console.log("GET /apitechu/v1/hello");
      res.send({"msg":"Hola desde API TechU"});
    }

)

app.get("/apitechu/v1/users",userController.getUsersV1) //obtención de lista de usuarios
app.get("/apitechu/v2/users",userController.getUsersV2) //obtención de lista de usuarios v2
app.get("/apitechu/v2/users/:id",userController.getUserByIdV2) //obtención de un usuario v2

app.get("/apitechu/v1/accounts/:idusuario",accountController.getAccountByIdV1) //obtención cuentas de un usuario v1

app.delete("/apitechu/v1/users/:id",userController.deleteUsersV1) //borrado de usuario

app.post("/apitechu/v1/users",userController.createUsersV1) //creación de usuario
app.post("/apitechu/v2/users",userController.createUsersV2) //creación de usuario v2

app.post("/apitechu/v1/login",authController.loginUserV1) //loggin de usuario v1
app.post("/apitechu/v2/login",authController.loginUserV2) //loggin de usuario v2

app.post("/apitechu/v1/logout/:id",authController.logoutUserV1) //logout de usuario v1
app.post("/apitechu/v2/logout/:id",authController.logoutUserV2) //logout de usuario v2

app.post("/apitechu/v1/monstruo/:p1/:p2",

    function(req,res){ //vamos a visualizar todos los modos de pasar parámetros

        console.log("Parámetros");
        console.log(req.params);

        console.log("Query String");
        console.log(req.query);

        console.log("Headers");
        console.log(req.headers);

        console.log("Body");
        console.log(req.body);

    }
)
/*
app.post("/apitechu/v1/users",

    function(req,res){

        console.log("POST /apitechu/v1/users");

        console.log(req.headers);
        console.log(req.headers.first_name);
        console.log(req.headers.last_name);
        console.log(req.headers.email);

        var newUser = {
          "first_name" : req.headers.first_name,
          "last_name" : req.headers.last_name,
          "email" : req.headers.email,
        }

        console.log(newUser);

        var users = require('./usuarios.json');
        users.push(newUser);
        console.log("Usuario añadido al array");

        const fs = require('fs');
        var jsonUserData = JSON.stringify(users); // pasamos la estructurta a etring para poder almacenarla en el fichero

        fs.writeFile("./usuarios.json",jsonUserData,"utf8",
          function(err){
              if (err) {
                  console.log(err);
              }else{
                  console.log("Usuario escrito en fichero");
              }
          }

        )

        res.send({"msg":"usuario creado"});

    }
) */
