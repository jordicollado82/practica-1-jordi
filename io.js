const fs = require('fs');

function writeUserDataToFile(data){
//    console.log("writeUserDataToFile");

    var jsonUserData = JSON.stringify(data); // pasamos la estructurta a etring para poder almacenarla en el fichero

      fs.writeFile("./usuarios.json",jsonUserData,"utf8",
        function(err){
            if (err) {
                console.log(err);
              }else{
                console.log("Fichero de usuarios actualizado");
              }
            }
      )
}

module.exports.writeUserDataToFile = writeUserDataToFile; //asocio la función con algo que estoy exportando
