const bcrypt = require('bcrypt');

function hash(data){
    console.log("Hashing data");
    return bcrypt.hashSync(data,10); //retorna un string

}

function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed) {
 console.log("Checking password");

 // Returns boolean
 return bcrypt.compareSync(passwordFromUserInPlainText, passwordFromDBHashed);
}

module.exports.hash = hash; //asocio la función con algo que estoy exportando
module.exports.checkPassword = checkPassword;
