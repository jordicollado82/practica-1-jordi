# DOCKER NODE IMAGE BASE
FROM node

# WORKING DIRECTORY
WORKDIR /apitechu

# COPY PROJECT
ADD . /apitechu

# INSTALL DEPENDENCIES
RUN npm install --only=prod

# Expose node port
EXPOSE 3000

# CMD INIT
CMD ["node", "server.js"]
